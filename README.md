# Python Nbody Integrator


Se incluyen los siguientes algoritmos:

    - Hybrid symplectic/Bulirsch-Stoer integrato

## Instalación

La instalación se debe realizar de forma editable, ejecutando el siguiente comando en el root del repositorio,

```python
pip install -e .
```

## Preparar workspace

Inicialmente se debe tener una carpeta de trabajo (workspace) en donde se almacenaran todas las corridas con su respectiva
información. Para ejemplificar consideremos el proyecto *nbody_template*, se crea una carpeta con su nombre dentro
del workspace y su estructura es la siguiente:

```
nbody_simulation
+-- config
|   +-- params.json
|   +-- init_config.h5
+-- data
+-- info
+-- simulation
+-- README.md

```

A continuación se explica cada uno de los folders:

    - `config`: En este folder se encuentra todos los datos referentes a la evolución del sistema (parametros y configuración inicial),
        en el archivo `params.json` se encuentran los parametros de la simulación y en el archivo `init_config.h5` se contiene
        la configuración inicial del sistema. La estructura de estos archivo se explica en ... #TODO.

    - `data`: Este folder contiene información referente a las configuraciones del sistema, las cuales no se encuentran
        en el formato utilizado por el simulador. Esta información puede ser preprocesada con algún helper para convertirla
        al formato por defecto.

    - `info`: En este folder se información referente al estado de la simulación y visualizaciones refentes a la
        simulación por ejemplo: animaciones del sistema, graficas de energía, excentricidad vs semieje mayor,
        masa vs semieje mayor, número de objetos, etc.

    - `simulation`: Se encuentran los outputs y backups.

    - `README.md`: Información de ususario referente al sistema.

## Ejecución inicial

Realizar los siguientes pasos:

    -   Ejecutar el archivo parameters.py para cear archivo YAML con parametros y estructura definida en el archivo.
        Este archivo se puede general manualmente respetando la estructura (Definiti Estructura) # TODO

    -   Ejecutar el archivo random_init.py para cear archivo hdf5 con toda la información inicial de un sistema generado
        aleatoriamente, bajo ciertas restricciones definidas en el script. Esto cambiara por un modulo de generador de
        sistemas iniciales. Crear caso que lea apartir de un txt, excel, etc...


    -   Ejecutar el comando python run.py --verbosity para tener retroalimentación.contiene

    -   Para obtener visuales de la simulación, se utiliza el notebook Indicadores.
