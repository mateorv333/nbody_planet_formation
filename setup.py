import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="PlanetFormation",
    version="0.0.1",
    author="Mateo Restrepo, Pablo Cuartas",
    author_email="mateo.restrepov@udea.edu.co, pablo.cuartas@udea.edu.co",
    description="Este paquete permite realizar simulaciones de formación planetaria, desde sintesis planetaria hasta su"
                "evolución.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mateorv333/nbody_planet_formation",
    packages=['NBodyF'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=['numpy>=1.17.2', 'h5py>=2.10.0', 'matplotlib>=3.1.1', 'pandas>=0.25.1']
)