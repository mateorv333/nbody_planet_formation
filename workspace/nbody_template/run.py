# -*- coding: utf-8 -*-
import yaml
import warnings
import os
import math
from tqdm import tqdm
from math import pi
import numpy as np

from NBodyF.utils.args import *
from NBodyF.utils.io import load_initial_system, create_hdf5, insert_group_hdf5
from NBodyF.core.integrators import hybrid_symplectic_integrator_model_frag, hybrid_symplectic_integrator, leapfrog_step, hybrid_symplectic_integrator_frag
from NBodyF.core.forces import acceleration_calc_star
from NBodyF.core.fragmentation import chamber_fragmentation_model
from NBodyF.core.utils import remover, insert_js_model
from NBodyF.core.units import UA_to_LU
from NBodyF.core.default import js_model


if __name__ == '__main__':
    overwrite = False

    args = runner.parse_args()
    path_params = args.path_params
    path_init = args.path_init_config

    if args.overwrite:
        overwrite = True

    with open(path_params) as file:
        parameters = yaml.load(file)

    # Parametros Obligatorioas
    name = parameters['name']
    tf = parameters['tf']
    dt = parameters['dt']
    n1 = parameters['n1'] if 'n1' in parameters.keys() else 3. # No implmenetado cambiar en el codigo
    n2 = parameters['n2'] if 'n2' in parameters.keys() else 0.4
    record = parameters['record']

    ## Chequear que el record sea un numero entero
    assert (type(record) == int)

    # Definir Removedor de Objetos
    dmin = parameters['dmin'] if 'dmin' in parameters.keys() else 0.2 * UA_to_LU
    remov = remover(dmin, 10 * UA_to_LU)


    # Definir Integrador
    integ = parameters['integrator']
    if integ == "HS_BS":
        integrator = hybrid_symplectic_integrator
    elif integ == "LF":
        integrator = leapfrog_step(acceleration_calc_star)
    elif  integ == "HS_BS_FRAG":
        integrator = hybrid_symplectic_integrator_model_frag(model_frag=chamber_fragmentation_model())# hybrid_symplectic_integrator_frag
    else:
        raise NotImplementedError

    # Parametros Opcionales
    ## Modelo Jupiter-Saturno
    add_jup_sat = parameters['add_jup_sat'] if 'add_jup_sat' in parameters.keys() else False

    if add_jup_sat:
        if 'model_jup_sat' not in parameters.keys():
            raise ValueError("No se ha definido el modelo JS")
        else:
            model_jup_sat = parameters['model_jup_sat']
            if model_jup_sat in js_model.keys():
                modeljs = js_model[model_jup_sat]
            else:
                raise ValueError("El modelo {} no ha sido identificado. Los modelos soportados son: {}".format(model_jup_sat, js_model.keys()))

    # Inicialización de Sistema
    r, v, m, sys_par = load_initial_system(path_init)

    m_star = sys_par["m_star"]
    m_o = sys_par["m_o"]
    density = sys_par["density"]

    radius = (m / (4. / 3. * pi * density)) ** (1. / 3.)
    denst = density * np.ones(m.shape)

    ## Agregar Jupiter y Saturno
    if add_jup_sat:
        r, v, m, radius, denst = insert_js_model(modeljs, r, v, m, radius, denst, m_star)

    # Creación de Archivo para guardar snapshots
    params = {**parameters, **sys_par}
    file_path = os.path.join(simulation_folder, name + ".h5")

    if os.path.exists(file_path):
        if overwrite:
            create_hdf5(file_path, params)
        else:
            warnings.warn("El archivo {} ya existe y no sera sobreescrito. En caso de estar seguro de sobreescribirlo ejecutar "
                          "el archivo con el flag --overwrite \n".format(file_path))
    else:
        create_hdf5(file_path, params)

    # Inicialización de Contadores
    time = 0.
    max_iter = math.ceil(tf / dt)
    nb = 0 # Número de Objetos
    ndiv = 0 # Número de Objetos Divergentes
    nstar = 0 # Número de Objetos absorbido por la estrella
    ncol_reb = 0 # Contador de colisiones-rebound
    ncol_frag = 0  # Contador de colisiones-fragmentacion
    ncol_merge = 0  # Contador de colisiones-merge

    print("El número total de iteraciones es {}".format(max_iter))

    for i in tqdm(range(max_iter)):
        if i%record == 0:
              insert_group_hdf5(file_path, r, v, m, m_star, radius = radius, density = denst, iteration = i // record, time = time, dt = dt)

        # HB
        # r, v = integrator(r, v, m, dt, m_star)

        #HB Frag
        r, v, m, radius, denst = integrator(r, v, m, radius, denst, dt, m_star)

        # Delete Objects
        index, m_star = remov(r, v, m, m_star)
        if len(index) > 0:
            print("Remove ", index)
            r = np.delete(r, index, axis=0)
            v = np.delete(v, index, axis=0)
            m = np.delete(m, index, axis=0)
            radius = np.delete(radius, index, axis=0)
            denst = np.delete(denst, index, axis=0)

        time += dt








