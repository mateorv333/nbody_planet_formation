# -*- coding: utf-8 -*-
import os
import yaml
import warnings

from NBodyF.utils.io import create_hdf5
from NBodyF.utils.args import *
from NBodyF.core.units import UA_to_LU

"""
    Parametros:

    -   dt : 
    -   tf :
    -   integrator : 
            * Hybrid symplectic/Bulirsch-Stoer integrator (HS_BS)
            * Bulirsch-Stoer integrator (BS)
    




"""

params_simul = dict(
                  name='template_hb_JS',
                  dt=6./365.,
                  tf=20.,
                  integrator='HS_BS_FRAG',
                  n1=3.,
                  n2=0.4,
                  dmin=0.2 * UA_to_LU,
                  record=10,
                  add_jup_sat=True,
                  model_jup_sat='JS'
                  )

if __name__ == '__main__':
    overwrite = False

    args = saver.parse_args()
    if args.overwrite:
        overwrite = True

    if os.path.exists(path_params_simul):
        if overwrite:
            with open(path_params_simul, 'w') as file:
                yaml.dump(params_simul, file)
        else:
            warnings.warn("El archivo {} ya existe y no sera sobreescrito. En caso de estar seguro de sobreescribirlo ejecutar "
                          "el archivo con el flag --overwrite \n".format(path_params_simul))
    else:
        with open(path_params_simul, 'w') as file:
            yaml.dump(params_simul, file)
