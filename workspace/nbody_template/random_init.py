# -*- coding: utf-8 -*-
import os
import warnings

from NBodyF.core.initial_conditions import random_system
from NBodyF.core.units import UA_to_LU, g_to_MU
from NBodyF.core.constants import den, M_merc, M_solar
from NBodyF.core.default import *
from NBodyF.utils.io import create_hdf5, insert_group_hdf5
from NBodyF.utils.args import *

"""
Pasar los parametros convertidos
"""

params_system = dict(
    n_g=10,
    n_p=20,
    a_min=3 * UA_to_LU,
    a_max=4 * UA_to_LU,
    m_star=1. * M_solar * g_to_MU,
    m_o=M_merc * g_to_MU,
    density=den,
    dim=2,
    fmg_min=1,
    fmg_max=5,
    fmp_min=0.01,
    fmp_max=0.0333
)

if __name__ == '__main__':
    overwrite = False

    args = saver.parse_args()
    if args.overwrite:
        overwrite = True

    r, v, m, radius, density = random_system(**params_system)

    structure = "+-- data\n" \
                "|   +-- 000*\n" \
                "|       +-- time\n" \
                "|       +-- dt\n" \
                "|       +-- planets\n" \
                "|           +-- position\n" \
                "|               +-- x\n" \
                "|               +-- y\n" \
                "|               +-- z\n" \
                "|           +-- velocity\n" \
                "|               +-- x\n" \
                "|               +-- y\n" \
                "|               +-- z\n" \
                "|           +-- mass\n" \
                "+-- date\n" \
                "+-- parameters\n" \
                "+-- params\n"

    if os.path.exists(path_init_config):
        if overwrite:
            create_hdf5(path_init_config, params_system, structure)
            insert_group_hdf5(path_init_config, r, v, m, radius, density)

        else:
            warnings.warn("El archivo {} ya existe y no sera sobreescrito. En caso de estar seguro de sobreescribirlo ejecutar "
                          "el archivo con el flag --overwrite \n".format(path_init_config))
    else:
        create_hdf5(path_init_config, params_system, structure)
        insert_group_hdf5(path_init_config, r, v, m, radius, density)
