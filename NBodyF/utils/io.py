# -*- coding: utf-8 -*-
import h5py
import os
import datetime
import json
import numpy as np
import ast

def create_hdf5(path, parameters=None, structure=None):
    """


    :param path:
    :param parameters:
    :return:
    """

    dirname = os.path.dirname(path)
    os.makedirs(dirname, exist_ok=True)

    with h5py.File(path, 'w') as f:
        f.attrs["base_path"] = b"/data/{}/"
        f.attrs["planets_path"] = b"planets/"
        f.attrs["author"] = b"Mateo Restrepo Villa <mateorv333@gmail.com>"

        f.attrs["date"] = bytes(
            datetime.datetime.now().replace(tzinfo=datetime.timezone.utc).strftime("%Y-%M-%d %T %z"), "utf-8")

        f.attrs["iteration_format"] = "/data/{0:08d}/"

        if parameters is not None:
            f.attrs["parameters"] = json.dumps(parameters)

        if structure is not None:
            f.attrs["structure"] = structure

    # TODO: Addd units information

    return f


def insert_group_hdf5(path, r, v, m, m_star, radius=None, density=None, iteration=None, time=None, dt=None):
    """


    :param path:
    :param iteration:
    :param time:
    :param dt:
    :param r:
    :param v:
    :param m:
    :return:
    """
    # Dimensión de la simulación
    dim = r.shape[1]

    with h5py.File(path, 'a') as f:
        # Creación de subgrupo para t
        if iteration is None:
            iteration = 0

        if time is not None:
            f.attrs["last_time"] = time

        f.attrs["last_iteration"] = iteration

        g = f.create_group(f.attrs["iteration_format"].format(iteration))

        g.attrs["m_star"] = m_star

        if time is not None:
            g.attrs["time"] = time

        if dt is not None:
            g.attrs["dt"] = dt

        # Creación de subsubgrupo para planetas
        planets = g.create_group(f.attrs["planets_path"])

        # Guardar cantidades vectoriales
        for index, direction in enumerate("xyz"[:dim]):
            # Posición
            position = planets.create_dataset(f"position/{direction}", data=r[:, index])

            # Velocidad
            velocity = planets.create_dataset(f"velocity/{direction}", data=v[:, index])

        # Guardar Masa
        mass = planets.create_dataset("mass", data=m[:, 0])

        # Guardar Radio
        if radius is not None:
            rad = planets.create_dataset("radius", data=radius[:, 0])

        # Guardar Densidad
        if density is not None:
            den = planets.create_dataset("density", data=density[:, 0])



def save_system_to_hdf5(f: h5py.File, iteration, time, dt, r, v, m, m_star):
    """


    :param f:
    :param iteration:
    :param time:
    :param dt:
    :param r:
    :param v:
    :param m:
    :return:
    """

    # Dimensión de la simulación
    dim = r.shape[1]

    # Creación de subgrupo para t
    g = f.create_group(f.attrs["iteration_format"].format(iteration))
    g.attrs["time"] = time
    g.attrs["dt"] = dt
    g.attrs["m_star"] = m_star

    # Creación de subsubgrupo para planetas
    planets = g.create_group(f.attrs["planets_path"])

    # Guardar cantidades vectoriales
    for index, direction in enumerate("xyz"[:dim]):
        # Posición
        position = planets.create_dataset(f"position/{direction}", data=r[:, index])
        position.attrs["description"] = "Posicion en {} de las particulas".format(direction)

        # Velocidad
        velocity = planets.create_dataset(f"velocity/{direction}", data=v[:, index])
        velocity.attrs["description"] = "Velocidad en {} de las particulas".format(direction)

    # Guardar Masa
    mass = planets.create_dataset("mass", data=m[:, 0])
    mass.attrs["description"] = "Masa de las particulas"


def save_iteration(file_path, iteration, time, dt, r, v, m):
    """


    :param file_path:
    :param iteration:
    :param time:
    :param dt:
    :param r:
    :param v:
    :param m:
    :return:
    """
    f = create_hdf5(file_path.format(iteration))
    save_system_to_hdf5(f, iteration, time, dt, r, v, m)
    f.close()

    # TODO: Guardar todo en un mismo hdf5 con subgrupos temporales


def load_initial_system(file_path):
    """


    :param file_path:
    :return:
    """
    with h5py.File(file_path, 'r') as f:
        params_system = ast.literal_eval(f.attrs["parameters"])

        iters = [k for k in f["data"].keys()]
        iteration = "{0:08d}".format(0)

        if iteration not in iters:
            raise ValueError("La iteración {} no se encuentra en el historial".format(user_iter))

        k = iteration
        mass = f["data/{}/planets/mass".format(k)]
        x = f["data/{}/planets/position/x".format(k)]
        y = f["data/{}/planets/position/y".format(k)]
        vx = f["data/{}/planets/velocity/x".format(k)]
        vy = f["data/{}/planets/velocity/y".format(k)]

        n = len(x[:])

        r = np.concatenate((x[:], y[:]), axis=1).reshape(n, 2, 1)
        v = np.concatenate((vx[:], vy[:]), axis=1).reshape(n, 2, 1)
        m = np.array(mass[:]).reshape(n, 1)

    return r, v, m, params_system


def load_system(file_path, iteration=None, lag=0, radius=False):
    """


    :param file_path:
    :return:
    """
    r = list()
    v = list()
    m = list()
    tm = list()
    ms = list()
    if radius:
        rad = list()

    with h5py.File(file_path, 'r') as f:
        try:
            params_system = ast.literal_eval(f.attrs["parameters"])
        except:
            params_system = json.loads(f.attrs["parameters"])

        params_system["last_time"] = f.attrs["last_time"]
        params_system["last_iteration"] = f.attrs["last_iteration"]

        iters = [k for k in f["data"].keys()]
        iters = iters[:len(iters) - lag]

        if iteration is not None:
            if iteration == -1:
                user_iter = iters[-1]
            else:
                user_iter = "{0:08d}".format(iteration)

            if user_iter not in iters:
                raise ValueError("El registro {} no se encuentra en el historial".format(user_iter))

            iters = [user_iter]

        for k in iters:
            mstar = f["data/{}".format(k)].attrs["m_star"]
            time = f["data/{}".format(k)].attrs["time"]
            mass = f["data/{}/planets/mass".format(k)]
            x = f["data/{}/planets/position/x".format(k)]
            y = f["data/{}/planets/position/y".format(k)]
            vx = f["data/{}/planets/velocity/x".format(k)]
            vy = f["data/{}/planets/velocity/y".format(k)]

            n = len(x[:])

            r.append(np.concatenate((x[:], y[:]), axis=1).reshape(n, 2, 1))
            v.append(np.concatenate((vx[:], vy[:]), axis=1).reshape(n, 2, 1))
            tm.append(time)
            ms.append(mstar)
            if radius:
                rads = f["data/{}/planets/radius".format(k)]
                rad.append(rads)

            m.append(np.array(mass[:]).reshape(n, 1))
    if radius:
        return r, v, m, rad, ms, tm, params_system
    else:
        return r, v, m, ms, tm, params_system