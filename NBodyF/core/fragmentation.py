# -*- coding: utf-8 -*-
from NBodyF.core.constants import den, den1
from math import pi, sin, cos

import numpy as np


def collision_merge_model(ri, rj, vi, vj, mi, mj, density=den ):
    """
    Agregate max density, densidad un vector de simulacion
    """
    m = mi + mj
    r = (mi * ri + mj * rj) / m
    v = (mi * vi + mj * vj) / m
    radius = (m / (4. / 3. * pi * density)) ** (1. / 3.)

    return r.reshape(1, 2, 1), v.reshape(1, 2, 1), m.reshape(1, 1), radius.reshape(1, 1)


def chamber_fragmentation_model(n_frag=4, np_frag=4, fac=4, denc=den, c_st=1.8,
                                    m_min=1.4e-9, den1=den1, c1=2.43,
                                    c2=-0.0408, c3=1.86, c4=1.08):
    pi_3_4 = 3. / (4. * pi)
    exp_1_3 = 1. / 3.
    d_pi_3_4 = pi_3_4 / den1
    dc_pi_3_4 = pi_3_4 / denc
    k_q_as_0 = 0.8 * c_st * pi * den1
    pi2 = 2 * pi
    f_3_4 = 3. / 4.

    def model_frag(ri, rj, vi, vj, mi, mj, radi, radj, deni, denj):
        if mi >= mj:
            mt = mi[0]
            mp = mj[0]
            rt = ri
            rp = rj
            vt = vi
            vp = vj
            radt = radi[0]
            radp = radj[0]
            dent = deni[0]
            denp = denj[0]
        else:
            mt = mj[0]
            mp = mi[0]
            rt = rj
            rp = ri
            vt = vj
            vp = vi
            radt = radj[0]
            radp = radi[0]
            dent = denj[0]
            denp = deni[0]

        total_mas = mt + mp

        sum_rad = radt + radp
        gamma = mp / mt

        # Relative Position Velocity
        rrel = ri - rj
        vrel = vi - vj
        v_mag = np.linalg.norm(vrel)
        v_esc_mut = ((2 * total_mas / sum_rad)) ** 0.5  # Mutual escape velocity

        m_r = np.linalg.norm(rrel)
        m_v = np.linalg.norm(vrel)

        acr_rv = rrel[0] * vrel[1] - rrel[1] * vrel[0]
        sin_th = abs(acr_rv[0]) / (m_r * m_v)

        fc_c = radt / (radt + radp)

        if sin_th > fc_c:
            print("Is a Posible Hit-and-run Collision")

            v_imp = (v_mag ** 2 + v_esc_mut ** 2) ** 0.5

            gam = ((1 - gamma) / (1 + gamma)) ** 2
            v_crit = c1 * gam * (1 - sin_th) ** 2.5 + c2 * gam + c3 * (1 - sin_th) ** 2.5 + c4

            if v_imp <= v_crit:
                print("Merge")

                m1 = total_mas
                r1 = (mt * rt + mp * rp) / total_mas
                v1 = (mt * vt + mp * vp) / total_mas
                rad1 = (dc_pi_3_4 * total_mas) ** exp_1_3

                m_c = np.array([[m1]])
                r_c = np.array([r1])
                v_c = np.array([v1])
                rad_c = np.array([[rad1]])
                den_c = np.array([[denc]])

            else:
                print("Is a Hit-and-run Collision")

                # Impact energy per unit mass (Q)
                mu = mt * mp / total_mas
                q = 0.5 * mu * v_mag ** 2 / total_mas

                # Critical Value (Q*)
                rc1 = (d_pi_3_4 * total_mas) ** exp_1_3
                q_as_0 = k_q_as_0 * rc1 ** 2

                ## Oblique and Unequal Masses
                if sin_th >= 1 - 2 * radp / (sum_rad):
                    f_temp_r = (radp + radt) / radp
                    alpha = f_temp_r ** 2 * (1 - sin_th) ** 2 * (f_3_4 - 0.25 * f_temp_r * (1 - sin_th))
                else:
                    alpha = 1
                # alpha = 0.8

                mu_alp = alpha * mt * mp / (mt + alpha * mp)
                q_as = (mu / mu_alp) ** 1.5 * (0.25 * (1 + gamma) ** 2 / gamma) * q_as_0

                # M1 Largest Remnant from the collision
                if q < 1.8 * q_as:
                    m1 = total_mas * (1 - 0.5 * q / q_as)
                else:
                    m1 = 0.1 * total_mas * (q / (1.8 * q_as)) ** (-1.5)

                r1 = (mt * rt + mp * rp) / total_mas

                if m1 < mt:
                    print("Porjectile no survive intact")

                    m_rem = total_mas - m1

                    m_f = m_rem / np_frag
                    rad_f = (dc_pi_3_4 * m_f) ** exp_1_3
                    rad_1 = (dc_pi_3_4 * m1) ** exp_1_3

                    v1 = (mt * vt + mp * vp) / (m1 + m_f * n_frag)

                    m_frag = list()
                    r_frag = list()
                    v_frag = list()
                    rad_frag = list()
                    den_frag = list()

                    vft = np.array([[0], [0]])
                    v_esc_tp = 0.8*v_esc_mut # 1.05 * v_esc_mut  # ((2 * total_mas / rc1)) ** 0.5  # Mutual escape velocity ??

                    dtheta = pi2 / np_frag
                    init_deg = np.random.uniform(0, pi2, 1)
                    for i in range(1, np_frag + 1):
                        th = init_deg + i * dtheta

                        unit_r = np.array([[cos(th)], [sin(th)]])
                        unit_v = np.array([[-sin(th)], [cos(th)]]) #np.array([[cos(th)], [sin(th)]])

                        rf = r1 + fac * rad_1 * unit_r
                        vf = v1 + v_esc_tp * unit_v

                        # vft = vft + vf

                        r_frag.append(rf)
                        v_frag.append(vf)
                        m_frag.append([m_f])
                        rad_frag.append([rad_f])
                        den_frag.append([denc])

                    # vft[vft < 1e-13] = 0.

                    r_frag = np.array(r_frag)
                    v_frag = np.array(v_frag)
                    m_frag = np.array(m_frag)
                    rad_frag = np.array(rad_frag)
                    den_frag = np.array(den_frag)

                    v1 = (mt * vt + mp * vp - m_f * vft) / (m1)

                    m_c = np.concatenate((np.array([[m1]]), m_frag))
                    r_c = np.concatenate((np.array([r1]), r_frag))
                    v_c = np.concatenate((np.array([v1]), v_frag))
                    rad_c = np.concatenate((np.array([[rad_1]]), rad_frag))
                    den_c = np.concatenate((np.array([[denc]]), den_frag))

                else:
                    print("Porjectile survive intact with little fragmentation")

                    m1 = mt
                    r1 = rt
                    v1 = vt
                    rad1 = radt

                    f_temp_r = (radp + radt) / radt
                    beta = f_temp_r ** 2 * (1 - sin_th) ** 2 * (f_3_4 - 0.25 * f_temp_r * (1 - sin_th))

                    mass_beta = mp + beta * mt

                    # Critical Value (Q*)
                    rc1 = (d_pi_3_4 * mass_beta) ** exp_1_3
                    q_as_0 = k_q_as_0 * rc1 ** 2

                    ## Oblique and Unequal Masses
                    gamma = beta * mt / mp
                    q_as = (0.25 * (1 + gamma) ** 2 / gamma) * q_as_0

                    ## Impact Energy
                    mu_bet = beta * mt * mp / mass_beta
                    q = 0.5 * mu_bet * v_mag ** 2 / mass_beta

                    # Mp1 Largest Remnant from the projectile
                    if q < 1.8 * q_as:
                        mp1 = mass_beta * (1 - 0.5 * q / q_as)
                    else:
                        mp1 = 0.1 * mass_beta * (q / (1.8 * q_as)) ** (-1.5)

                    mp_rem = mp - mp1
                    if mp_rem < m_min:
                        print("Merge projectile fragmentation")
                        mp1 = mp
                        rp1 = rp
                        vp1 = vp
                        radp1 = radp

                        m_c = np.array([[m1], [mp1]])
                        r_c = np.array([r1, rp1])
                        v_c = np.array([v1, vp1])
                        rad_c = np.array([[rad1], [radp1]])
                        den_c = np.array([[denc], [denc]])

                    else:
                        print("Total projectile fragmentation")
                        # Generatign Fragments

                        m_f = mp_rem / np_frag
                        rad_f = (dc_pi_3_4 * m_f) ** exp_1_3
                        radp1 = (dc_pi_3_4 * mp1) ** exp_1_3

                        m_frag = list()
                        r_frag = list()
                        v_frag = list()
                        rad_frag = list()
                        den_frag = list()

                        vft = np.array([[0], [0]])
                        #                 v_esc_tp = 1.05 * (2 * (mt + mp) / rad)

                        v_esc_tp = 0.8*v_esc_mut # 1.05 * v_esc_mut  # ((2 * total_mas / sum_rad)) ** 0.5  # Mutual escape velocity ??

                        dtheta = 2 * pi / np_frag
                        init_deg = np.random.uniform(0, pi2, 1)
                        for i in range(1, np_frag + 1):
                            th = init_deg + i * dtheta

                            unit_r = np.array([[cos(th)], [sin(th)]])
                            unit_v = np.array([[-sin(th)], [cos(th)]])

                            rf = r1 + fac * rad1 * unit_r
                            vf = v1 + v_esc_tp * unit_v

                            vft = vft + vf

                            r_frag.append(rf)
                            v_frag.append(vf)
                            m_frag.append([m_f])
                            rad_frag.append([rad_f])
                            den_frag.append([denc])

                        vft[vft < 1e-13] = 0.

                        r_frag = np.array(r_frag)
                        v_frag = np.array(v_frag)
                        m_frag = np.array(m_frag)
                        rad_frag = np.array(rad_frag)
                        den_frag = np.array(den_frag)

                        rp1 = rp
                        vp1 = (mp * vp - m_f * vft) / mp1

                        m_c = np.concatenate((np.array([[m1], [mp1]]), m_frag))
                        r_c = np.concatenate((np.array([r1, rp1]), r_frag))
                        v_c = np.concatenate((np.array([v1, vp1]), v_frag))
                        rad_c = np.concatenate((np.array([[rad1], [radp1]]), rad_frag))
                        den_c = np.concatenate((np.array([[denc], [denc]]), den_frag))

        else:
            print("Is a Nonhit-and-run Collision")

            # Impact energy per unit mass (Q)
            mu = mt * mp / total_mas
            q = 0.5 * mu * v_mag ** 2 / total_mas

            # Critical Value (Q*)
            rc1 = (d_pi_3_4 * total_mas) ** exp_1_3
            q_as_0 = k_q_as_0 * rc1 ** 2

            ## Oblique and Unequal Masses
            if sin_th >= 1 - 2 * radp / (sum_rad):
                f_temp_r = (radp + radt) / radp
                alpha = f_temp_r ** 2 * (1 - sin_th) ** 2 * (f_3_4 - 0.25 * f_temp_r * (1 - sin_th))
            else:
                alpha = 1
            # alpha = 0.8

            mu_alp = alpha * mt * mp / (mt + alpha * mp)

            # print(mu_alp, alpha, mt, mp)

            q_as = (mu / mu_alp) ** 1.5 * (0.25 * (1 + gamma) ** 2 / gamma) * q_as_0

            # M1 Largest Remnant from the collision
            if q < 1.8 * q_as:
                m1 = total_mas * (1 - 0.5 * q / q_as)
            else:
                m1 = 0.1 * total_mas * (q / (1.8 * q_as)) ** (-1.5)

            r1 = (mt * rt + mp * rp) / total_mas

            m_rem = total_mas - m1
            if m_rem < m_min:
                print("Merge")

                m1 = total_mas
                rad1 = (dc_pi_3_4 * m1) ** exp_1_3
                v1 = (mt * vt + mp * vp) / m1

                m_c = np.array([[m1]])
                r_c = np.array([r1])
                v_c = np.array([v1])
                rad_c = np.array([[rad1]])
                den_c = np.array([[denc]])

            #         return m_c, r_c, v_c, rad_c
            else:
                print("Fragmentation")

                m_f = m_rem / n_frag
                rad_f = (dc_pi_3_4 * m_f) ** exp_1_3
                rad_1 = (dc_pi_3_4 * m1) ** exp_1_3

                v1 = (mt * vt + mp * vp) / (m1 + m_f * n_frag)

                m_frag = list()
                r_frag = list()
                v_frag = list()
                rad_frag = list()
                den_frag = list()

                # vft = np.array([[0], [0]])
                v_esc_tp = 0.8*v_esc_mut # 1.05 * v_esc_mut  # ((2 * total_mas / rc1)) ** 0.5  # Mutual escape velocity ??

                dtheta = 2 * pi / n_frag
                init_deg = np.random.uniform(0, pi2, 1)
                for i in range(1, n_frag + 1):
                    th = init_deg + i * dtheta

                    unit_r = np.array([[cos(th)], [sin(th)]])
                    unit_v = np.array([[-sin(th)], [cos(th)]]) # np.array([[cos(th)], [sin(th)]])

                    rf = r1 + fac * rad_1 * unit_r
                    vf = v1 + v_esc_tp * unit_v

                    # vft = vft + vf

                    r_frag.append(rf)
                    v_frag.append(vf)
                    m_frag.append([m_f])
                    rad_frag.append([rad_f])
                    den_frag.append([denc])

                # vft[vft < 1e-13] = 0.

                r_frag = np.array(r_frag)
                v_frag = np.array(v_frag)
                m_frag = np.array(m_frag)
                rad_frag = np.array(rad_frag)
                den_frag = np.array(den_frag)

                # v1 = (mt * vt + mp * vp - m_f * vft) / (m1)

                m_c = np.concatenate((np.array([[m1]]), m_frag))
                r_c = np.concatenate((np.array([r1]), r_frag))
                v_c = np.concatenate((np.array([v1]), v_frag))
                rad_c = np.concatenate((np.array([[rad_1]]), rad_frag))
                den_c = np.concatenate((np.array([[denc], [denc]]), den_frag))

        return r_c, v_c, m_c, rad_c, den_c

    return model_frag


def collision_fragmentation_model(m_min, ):
    pass
#
#     def temp(ri, rj, vi, vj, mi, mj, density=den):
#
#     """
#     """
#     if mi >= mj:
#         mt = mi
#         mp = mj
#         rt = ri
#         rp = rj
#         vt = vi
#         vp = vj
#     else:
#         mt = mj
#         mp = mi
#         rt = rj
#         rp = ri
#         vt = vj
#         vp = vi
#
#     # Relative Velocity
#     v = vi - vj
#
#     # Impact energy per unit mass (Q)
#     mu = mt * mp / (mt + mp)
#     q = 0.5 * mu * v2 / (mt + mp)
#
#     # Critical Value (Q*)
#
#
#
#     m = mi + mj
#     r = (mi * ri + mj * rj) / m
#     v = (mi * vi + mj * vj) / m
#     radius = (m / (4. / 3. * pi * density)) ** (1. / 3.)
#
#     return r.reshape(1, 2, 1), v.reshape(1, 2, 1), m.reshape(1, 1), radius.reshape(1, 1)
#
#
#
