# -*- coding: utf-8 -*-
import numpy as np

from NBodyF.core.utils import calc_rcrit, close_encounters, dy_f, dy_f_2
from NBodyF.core.forces import acceleration_kepler, acceleration_calculator_dist_weighted, acceleration_calculator_dist_weighted_2
from NBodyF.core.fragmentation import collision_fragmentation_model, collision_merge_model

def leapfrog_step(acceleration_calculator):


    def temp(r, v, m, radius, dt, m_star):
        """ Leap Frog Integrator """

        # Positions at n+1/2
        r = r + 0.5 * dt * v

        # Calculate the forces at n+1/2
        a = acceleration_calculator(r, m, m_star)

        # Velocities and Posictions at n+1
        v = v + dt * a
        r = r + 0.5 * dt * v

        return r, v, m, radius
    return temp

def mult_midpoint(y, dy, dt, n_steps):
    h = dt/n_steps
    y0 = y
    y1 = y0 + h * dy(y0)

    for i in range(n_steps - 1):
        y2 = y0 + 2 * h * dy(y1)
        y0 = y1
        y1 = y2

    return 0.5 * (y1 + y0 + h * dy(y2))


def neville(y):
    """
    Richardson Interpolation based on Neville algorithm to find Lagrange polynomial and evaluate when "h -> 0"

    :param y: Array containing y values
    :return:
    """

    n, dim, _ = y.shape
    x = 2 * np.arange(1, n + 1)

    q = np.zeros((n, n - 1, dim))

    # Insert 'y' in the first column of the matrix 'q'
    q = np.concatenate((y.reshape(n, dim)[:, None], q), axis=1)

    for i in range(1, n):
        for j in range(1, i + 1):
            q[i, j] = (x[i] * q[i - 1, j - 1] - x[i - j] * q[i, j - 1]) / (x[i] - x[i - j])
    return q[n - 1, n - 1]


def bulirsch_stoer_integrator(y, dy, dt, tol=1e-13, max_iter=12):#8):

    values = list()
    values.append(mult_midpoint(y, dy, dt, 2))

    for n in range(2, max_iter + 1):
        values.append(mult_midpoint(y, dy, dt, 2 * n))

        # Compute mean of relative error
        e_r = np.mean(np.linalg.norm(values[-1][0] - values[-2][0], axis=1) / np.linalg.norm(values[-2][0], axis=1))
        e_v = np.mean(np.linalg.norm(values[-1][1] - values[-2][1], axis=1) / np.linalg.norm(values[-2][1], axis=1))
        e = 0.5 * (e_r + e_v)

        # print(e)

        if e < tol:
            break
        else:
            continue

    return np.array(values)

@np.vectorize
def k_ij(rij, r_crit):
    """


    :param rij:
    :param r_crit:
    :return:
    """
    x = (0.9 * r_crit) / (rij - 0.1 * r_crit)

    if x < 0.:
        return 0.
    elif x < 1.:
        return 1.
    else:
        return 1 / (2 - 2 * x + x * x)


def one_k_ij(rij, r_crit):
    return 1 - k_ij(rij, r_crit)


def hybrid_symplectic_integrator(r, v, m, dt, m_star=1.):
    """
    Implementation of hybrid sympletic integrator

    The hybrid symplectic algorithm is described in J.E.Chambers (1999) Monthly Notices of the RAS, vol 304, pp793.


    :param r:
    :param v:
    :param m:
    :param dt:
    :param m_star:
    :return:
    """
    dtby2 = dt * 0.5
    temp = dtby2 / m_star

    # Advance interaction Hamiltonian for H/2
    n_p = len(m)
    r_crit = calc_rcrit(r, v, m, m_star, 0.5 * dt)
    rc = np.maximum(r_crit.reshape(1, n_p), r_crit.reshape(n_p, 1))
    a = acceleration_calculator_dist_weighted(r, m, k_ij, rc.reshape(n_p, n_p, 1))

    v = v + dtby2 * a

    # Advance solar Hamiltonian for H/2

    mvsum = np.sum(m.reshape(n_p, 1, 1) * v, axis=0)

    r = r + temp * mvsum

    # Advance H_K for H (Keplerian)

    ## Save current coordinates
    r0 = np.copy(r)
    v0 = np.copy(v)



    # v = v + dt * a_k
    # r = r + dt * v

    # Positions at n+1/2
    r = r + 0.5 * dt * v

    a_k = acceleration_kepler(r, m_star)

    # Velocities and Posictions at n+1
    v = v + dt * a_k
    r = r + 0.5 * dt * v


    ## Detect Close Encounters
    ce, _, _, _ = close_encounters(r0, r, v0, v, dt, m, m_star)
    n_ce = np.sum(ce)

    if n_ce > 0:
        r_ce = r0[ce]
        v_ce = v0[ce]
        m_ce = m[ce]

        r_crit = calc_rcrit(r_ce, v_ce, m_ce, m_star, dt)
        rc = np.maximum(r_crit.reshape(1, n_ce), r_crit.reshape(n_ce, 1))

        y = np.array([r_ce, v_ce])
        dy = dy_f(rc.reshape(n_ce, n_ce, 1), np.vectorize(one_k_ij), m_ce, m_star)

        val = bulirsch_stoer_integrator(y, dy, dt)

        res = list()
        ves = list()
        for i in range(n_ce):
            res.append(neville(val[:, 0, i, :]).reshape(2, 1))
            ves.append(neville(val[:, 1, i, :]).reshape(2, 1))

        y = np.array([res, ves])

        r[ce] = y[0]
        v[ce] = y[1]

    # TODO: Implement Collision-Fragmentation Models

    # Advance solar Hamiltonian for H/2

    mvsum = np.sum(m.reshape(n_p, 1, 1) * v, axis=0)

    r = r + temp * mvsum

    # Advance interaction Hamiltonian for H/2

    r_crit = calc_rcrit(r, v, m, m_star, 0.5 * dt)
    rc = np.maximum(r_crit.reshape(1, n_p), r_crit.reshape(n_p, 1))
    a = acceleration_calculator_dist_weighted(r, m, k_ij, rc.reshape(n_p, n_p, 1))

    v = v + dtby2 * a

    return r, v


def hybrid_symplectic_integrator_2(r, v, m, dt, m_star=1.):
    """
    Implementation of hybrid sympletic integrator

    The hybrid symplectic algorithm is described in J.E.Chambers (1999) Monthly Notices of the RAS, vol 304, pp793.


    :param r:
    :param v:
    :param m:
    :param dt:
    :param m_star:
    :return:
    """


    dtby2 = dt * 0.5
    temp = dtby2 / m_star

    # Advance interaction Hamiltonian for H/2
    n_p = len(m)
    r_crit = calc_rcrit(r, v, m, m_star, 0.5 * dt)
    rc = np.maximum(r_crit.reshape(1, n_p), r_crit.reshape(n_p, 1))

    r_tp = np.copy(r)
    rc_tp = np.copy(r_crit)

    n, dim, _ = r.shape
    rij = r.reshape(1, n, dim) - r.reshape(n, 1, dim)
    distances_ij = np.linalg.norm(rij, axis=2, keepdims=True)
    distances_ij[np.arange(n), np.arange(n), :] = np.inf
    w = k_ij(distances_ij, rc)
    accelerations = rij / np.power(distances_ij, 3) * m.reshape(1, n, 1)# * w
    a = np.sum(accelerations, axis=1).reshape(n, dim, 1)

    print(distances_ij.shape)
    print(rc.shape)
    print(w.shape)
    print("Here")
    print(accelerations.shape)
    print(a.shape)

    print(distances_ij)

    a = acceleration_calculator_dist_weighted_2(r, m, w)

    v = v + dtby2 * a

    # Advance solar Hamiltonian for H/2

    mvsum = np.sum(m.reshape(n_p, 1, 1) * v, axis=0)

    r = r + temp * mvsum

    # Advance H_K for H (Keplerian)

    ## Save current coordinates
    r0 = np.copy(r)
    v0 = np.copy(v)



    # v = v + dt * a_k
    # r = r + dt * v

    # Positions at n+1/2
    r = r + 0.5 * dt * v

    a_k = acceleration_kepler(r, m_star)

    # Velocities and Posictions at n+1
    v = v + dt * a_k
    r = r + 0.5 * dt * v


    ## Detect Close Encounters
    ce, _, _, _ = close_encounters(r0, r, v0, v, dt, m, m_star)
    n_ce = np.sum(ce)

    if n_ce > 0:
        r_ = r_tp[ce]
        n_, dim, _ = r_.shape

        r_crit_ = rc_tp[ce]
        rc = np.maximum(r_crit_.reshape(1, n_), r_crit_.reshape(n_, 1))

        rij = r_.reshape(1, n_, dim) - r_.reshape(n_, 1, dim)
        distances_ij = np.linalg.norm(rij, axis=2, keepdims=True)
        distances_ij[np.arange(n_), np.arange(n_), :] = np.inf
        w = k_ij(distances_ij, rc)

        r_ce = r0[ce]
        v_ce = v0[ce]
        m_ce = m[ce]

        r_crit = calc_rcrit(r_ce, v_ce, m_ce, m_star, dt)
        # rc = np.maximum(r_crit.reshape(1, n_ce), r_crit.reshape(n_ce, 1))

        y = np.array([r_ce, v_ce])
        dy = dy_f_2(1 - w, m_ce, m_star)

        val = bulirsch_stoer_integrator(y, dy, dt)

        res = list()
        ves = list()
        for i in range(n_ce):
            res.append(neville(val[:, 0, i, :]).reshape(2, 1))
            ves.append(neville(val[:, 1, i, :]).reshape(2, 1))

        y = np.array([res, ves])

        r[ce] = y[0]
        v[ce] = y[1]

    # TODO: Implement Collision-Fragmentation Models

    # Advance solar Hamiltonian for H/2

    mvsum = np.sum(m.reshape(n_p, 1, 1) * v, axis=0)

    r = r + temp * mvsum

    # Advance interaction Hamiltonian for H/2

    r_crit = calc_rcrit(r, v, m, m_star, 0.5 * dt)
    rc = np.maximum(r_crit.reshape(1, n_p), r_crit.reshape(n_p, 1))
    a = acceleration_calculator_dist_weighted(r, m, k_ij, rc.reshape(n_p, n_p, 1))

    v = v + dtby2 * a

    return r, v


def hybrid_symplectic_integrator_frag(r, v, m, radius, density, dt, m_star):
    """
    Implementation of hybrid sympletic integrator

    The hybrid symplectic algorithm is described in J.E.Chambers (1999) Monthly Notices of the RAS, vol 304, pp793.


    :param r:
    :param v:
    :param m:
    :param dt:
    :param m_star:
    :return:
    """

    dtby2 = dt * 0.5
    temp = dtby2 / m_star

    # Advance interaction Hamiltonian for H/2
    n_p = len(m)
    r_crit = calc_rcrit(r, v, m, m_star, 0.5 * dt)
    rc = np.maximum(r_crit.reshape(1, n_p), r_crit.reshape(n_p, 1))
    a = acceleration_calculator_dist_weighted(r, m, np.vectorize(k_ij), rc.reshape(n_p, n_p, 1))

    v = v + dtby2 * a

    # Advance solar Hamiltonian for H/2

    mvsum = np.sum(m.reshape(n_p, 1, 1) * v, axis=0)

    r = r + temp * mvsum

    # Advance H_K for H (Keplerian)

    ## Save current coordinates
    r0 = np.copy(r)
    v0 = np.copy(v)

    # v = v + dt * a_k
    # r = r + dt * v

    # Positions at n+1/2
    r = r + 0.5 * dt * v

    a_k = acceleration_kepler(r, m_star)

    # Velocities and Posictions at n+1
    v = v + dt * a_k
    r = r + 0.5 * dt * v

    ## Detect Close Encounters
    ce, _, _, _ = close_encounters(r0, r, v0, v, dt, m, m_star)
    n_ce = np.sum(ce)

    if n_ce > 0:
        r_ce = r0[ce]
        v_ce = v0[ce]
        m_ce = m[ce]

        r_crit = calc_rcrit(r_ce, v_ce, m_ce, m_star, dt)
        rc = np.maximum(r_crit.reshape(1, n_ce), r_crit.reshape(n_ce, 1))

        y = np.array([r_ce, v_ce])
        dy = dy_f(rc.reshape(n_ce, n_ce, 1), np.vectorize(one_k_ij), m_ce, m_star)

        val = bulirsch_stoer_integrator(y, dy, dt)

        res = list()
        ves = list()
        for i in range(n_ce):
            res.append(neville(val[:, 0, i, :]).reshape(2, 1))
            ves.append(neville(val[:, 1, i, :]).reshape(2, 1))

        y = np.array([res, ves])

        r[ce] = y[0]
        v[ce] = y[1]

    ## Detect Close Encounters
    ce, ce_index, d_min, t_dmin = close_encounters(r0, r, v0, v, dt, m, m_star)
    n_ce = np.sum(ce)


    if n_ce > 0:
        del_obj = list()
        new_obj = list()

        ce_index = [c for _, c in sorted(zip(t_dmin, ce_index))]
        d_min = [c for _, c in sorted(zip(t_dmin, d_min))]
        t_dmin = sorted(t_dmin)

        for k, index in enumerate(ce_index):
            dmin = d_min[k]
            dhit = radius[index[0]] + radius[index[1]]

            if dmin <= dhit:
                del_obj.append(index[0])
                del_obj.append(index[1])
                # rn, vn, mn, radiusn = collision_merge_model(r[index[0]], r[index[1]], v[index[0]], v[index[1]], m[index[0]], m[index[1]])

                new_obj.append([rn, vn, mn, radiusn])

        if len(del_obj) > 0:
            # Elimina Objetos antes de la Colision
            r = np.delete(r, del_obj, axis=0)
            v = np.delete(v, del_obj, axis=0)
            m = np.delete(m, del_obj, axis=0)
            radius = np.delete(radius, del_obj, axis=0)

            # Inserta Objetos despues de la Colision
            for new in new_obj:
                r = np.concatenate((r, new[0]), axis=0)
                v = np.concatenate((v, new[1]), axis=0)
                m = np.concatenate((m, new[2]), axis=0)
                radius = np.concatenate((radius, new[3]), axis=0)

            n_p = len(m)


    # r, v, m, radius, dt, m_star = 1.

    # TODO: Implement Collision-Fragmentation Models

    # Advance solar Hamiltonian for H/2

    mvsum = np.sum(m.reshape(n_p, 1, 1) * v, axis=0)

    r = r + temp * mvsum

    # Advance interaction Hamiltonian for H/2

    r_crit = calc_rcrit(r, v, m, m_star, 0.5 * dt)
    rc = np.maximum(r_crit.reshape(1, n_p), r_crit.reshape(n_p, 1))
    a = acceleration_calculator_dist_weighted(r, m, np.vectorize(k_ij), rc.reshape(n_p, n_p, 1))

    v = v + dtby2 * a

    return r, v, m, radius, density


def hybrid_symplectic_integrator_model_frag(model_frag):

    def temp_f(r, v, m, radius, density, dt, m_star):
        """
        Implementation of hybrid sympletic integrator

        The hybrid symplectic algorithm is described in J.E.Chambers (1999) Monthly Notices of the RAS, vol 304, pp793.


        :param r:
        :param v:
        :param m:
        :param dt:
        :param m_star:
        :return:
        """

        dtby2 = dt * 0.5
        temp = dtby2 / m_star

        # Advance interaction Hamiltonian for H/2
        n_p = len(m)
        r_crit = calc_rcrit(r, v, m, m_star, 0.5 * dt)
        rc = np.maximum(r_crit.reshape(1, n_p), r_crit.reshape(n_p, 1))
        a = acceleration_calculator_dist_weighted(r, m, k_ij, rc.reshape(n_p, n_p, 1))

        v = v + dtby2 * a

        # Advance solar Hamiltonian for H/2

        mvsum = np.sum(m.reshape(n_p, 1, 1) * v, axis=0)

        r = r + temp * mvsum

        # Advance H_K for H (Keplerian)

        ## Save current coordinates
        r0 = np.copy(r)
        v0 = np.copy(v)

        # v = v + dt * a_k
        # r = r + dt * v

        # Positions at n+1/2
        r = r + 0.5 * dt * v

        a_k = acceleration_kepler(r, m_star)

        # Velocities and Posictions at n+1
        v = v + dt * a_k
        r = r + 0.5 * dt * v

        ## Detect Close Encounters
        ce, _, _, _ = close_encounters(r0, r, v0, v, dt, m, m_star)
        n_ce = np.sum(ce)

        if n_ce > 0:
            r_ce = r0[ce]
            v_ce = v0[ce]
            m_ce = m[ce]

            r_crit = calc_rcrit(r_ce, v_ce, m_ce, m_star, dt)
            rc = np.maximum(r_crit.reshape(1, n_ce), r_crit.reshape(n_ce, 1))

            y = np.array([r_ce, v_ce])
            dy = dy_f(rc.reshape(n_ce, n_ce, 1), np.vectorize(one_k_ij), m_ce, m_star)

            val = bulirsch_stoer_integrator(y, dy, dt)

            res = list()
            ves = list()
            for i in range(n_ce):
                res.append(neville(val[:, 0, i, :]).reshape(2, 1))
                ves.append(neville(val[:, 1, i, :]).reshape(2, 1))

            y = np.array([res, ves])

            r[ce] = y[0]
            v[ce] = y[1]

        ## Detect Close Encounters
        ce, ce_index, d_min, t_dmin = close_encounters(r0, r, v0, v, dt, m, m_star)
        n_ce = np.sum(ce)


        if n_ce > 0:
            del_obj = list()
            new_obj = list()

            ce_index = [c for _, c in sorted(zip(t_dmin, ce_index))]
            d_min = [c for _, c in sorted(zip(t_dmin, d_min))]

            for k, index in enumerate(ce_index):
                dmin = d_min[k]
                dhit = radius[index[0]][0] + radius[index[1]][0]

                if dmin <= dhit:
                    if not(index[0] in del_obj) and not(index[1] in del_obj):
                        # print("CE", dmin, dhit, radius[index[0]][0], radius[index[1]][0])
                        del_obj.append(index[0])
                        del_obj.append(index[1])
                        rn, vn, mn, radiusn, densityn = model_frag(r[index[0]], r[index[1]],
                                                                   v[index[0]], v[index[1]],
                                                                   m[index[0]], m[index[1]],
                                                                   radius[index[0]], radius[index[1]],
                                                                   density[index[0]], density[index[1]])

                        new_obj.append([rn, vn, mn, radiusn, densityn])

            # print(new_obj)
            if len(del_obj) > 0:
                # print(del_obj)
                # print(new_obj)
                # Elimina Objetos antes de la Colision
                r = np.delete(r, del_obj, axis=0)
                v = np.delete(v, del_obj, axis=0)
                m = np.delete(m, del_obj, axis=0)
                radius = np.delete(radius, del_obj, axis=0)
                density = np.delete(density, del_obj, axis=0)

                # Inserta Objetos despues de la Colision
                for new in new_obj:
                    r = np.concatenate((r, new[0]), axis=0)
                    v = np.concatenate((v, new[1]), axis=0)
                    m = np.concatenate((m, new[2]), axis=0)
                    radius = np.concatenate((radius, new[3]), axis=0)
                    density = np.concatenate((density, new[4]), axis=0)

                n_p = len(m)


        # r, v, m, radius, dt, m_star = 1.

        # TODO: Implement Collision-Fragmentation Models

        # Advance solar Hamiltonian for H/2

        mvsum = np.sum(m.reshape(n_p, 1, 1) * v, axis=0)

        r = r + temp * mvsum

        # Advance interaction Hamiltonian for H/2

        r_crit = calc_rcrit(r, v, m, m_star, 0.5 * dt)
        rc = np.maximum(r_crit.reshape(1, n_p), r_crit.reshape(n_p, 1))
        a = acceleration_calculator_dist_weighted(r, m, k_ij, rc.reshape(n_p, n_p, 1))

        v = v + dtby2 * a

        return r, v, m, radius, density
    return temp_f