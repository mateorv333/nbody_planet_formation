# -*- coding: utf-8 -*-
import numpy as np


def potential_energy_ps(r, m, m_star):
    distances_i0 = np.linalg.norm(r, axis=1, keepdims=False)
    potential_s = 1. / distances_i0 * m_star * m
    p_s = - np.sum(potential_s)
    return p_s


def potential_energy_pp(r, m):
    """

    :param r: Debe tener shape (N, dim, 1)
    :param m:
    :return:
    """

    n, dim, _ = r.shape
    rij = r.reshape(1, n, dim) - r.reshape(n, 1, dim)
    mij = m.reshape(1, n) * m.reshape(n, 1)
    distances_ij = np.linalg.norm(rij, axis=2, keepdims=True)
    distances_ij[np.arange(n), np.arange(n), :] = np.inf
    potential_p = 1. / distances_ij * mij.reshape(n, n, 1)
    p_p = -0.5 * np.sum(potential_p)
    return p_p


def potential_energy(r, m, m_star):
    p_s = potential_energy_ps(r, m, m_star)
    p_p = potential_energy_pp(r, m)
    p_e = p_s + p_p

    return p_e, p_s, p_p


def kinetic_energy(v, m, m_star):
    ke = 0

    # Particulas
    for i in range(len(m)):
        ke += 0.5 * m[i, 0] * np.linalg.norm(v[i]) ** 2

    # Sistema
    m_com = np.sum(m) + m_star
    v_com = np.sum(m.reshape(len(m), 1, 1) * v, axis=0) / m_com

    ke += 0.5 * m_com * np.linalg.norm(v_com) ** 2

    return ke


def total_energy(r, v, m, m_star):
    pe, ps, pp = potential_energy(r, m, m_star)
    ke = kinetic_energy(v, m, m_star)
    te = pe + ke

    return te, pe, ke, pp, ps


def acceleration_calculator(r, m):
    """

    :param r: Debe tener shape (N, dim, 1)
    :param m:
    :return:
    """

    n, dim, _ = r.shape
    rij = r.reshape(1, n, dim) - r.reshape(n, 1, dim)
    distances_ij = np.linalg.norm(rij, axis=2, keepdims=True)
    distances_ij[np.arange(n), np.arange(n), :] = np.inf
    accelerations = rij / np.power(distances_ij, 3) * m.reshape(n, 1, 1)
    a = np.sum(accelerations, axis=0).reshape(n, dim, 1)
    return a

def acceleration_calc_star(r, m, m_star):
    """


    :param m_star:
    :return:
    """

    n, dim, _ = r.shape

    # Aceleración debido a la intereacción planeta-planeta
    a_p = acceleration_calculator(r, m)

    # Aceleración debido a la intereacción planeta-estrella
    distances_i0 = np.linalg.norm(r, axis=1, keepdims=True)
    accelerations = - r / np.power(distances_i0, 3) * m_star

    # Aceleración total
    a_s = accelerations.reshape(n, dim, 1)
    a = a_p + a_s
    return a


def acceleration_kepler(r, m_star):
    """

    :param m_star:
    :return:
    """
    n, dim, _ = r.shape

    # Aceleración debido a la intereacción planeta-estrella
    distances_i0 = np.linalg.norm(r, axis=1, keepdims=True)
    accelerations = - r / np.power(distances_i0, 3) * m_star

    # Aceleración total
    a = accelerations.reshape(n, dim, 1)

    return a


def acceleration_calculator_dist_weighted(r, m, K, r_crit):
    """

    :param r: Debe tener shape (N, dim, 1)
    :param m:
    :param K:
    :return:
    """

    n, dim, _ = r.shape
    rij = r.reshape(1, n, dim) - r.reshape(n, 1, dim)
    distances_ij = np.linalg.norm(rij, axis=2, keepdims=True)
    distances_ij[np.arange(n), np.arange(n), :] = np.inf
    accelerations = rij / np.power(distances_ij, 3) * m.reshape(n, 1, 1) * K(distances_ij, r_crit)
    a = np.sum(accelerations, axis=0).reshape(n, dim, 1)
    return a


def acceleration_calculator_dist_weighted_2(r, m, w):
    """

    :param r: Debe tener shape (N, dim, 1)
    :param m:
    :param K:
    :return:
    """

    n, dim, _ = r.shape
    rij = r.reshape(1, n, dim) - r.reshape(n, 1, dim)
    distances_ij = np.linalg.norm(rij, axis=2, keepdims=True)
    distances_ij[np.arange(n), np.arange(n), :] = np.inf
    print(w)
    accelerations = rij / np.power(distances_ij, 3) * m.reshape(1, n, 1) * w
    a = np.sum(accelerations, axis=1).reshape(n, dim, 1)
    return a


def acceleration_calculator_dist_weighted_star_2(r, m, w, m_star):
    """


    :param m_star:
    :return:
    """
    n, dim, _ = r.shape

    # Aceleración debido a la intereacción planeta-planeta
    a_p = acceleration_calculator_dist_weighted_2(r, m, w)

    # Aceleración debido a la intereacción planeta-estrella
    distances_i0 = np.linalg.norm(r, axis=1, keepdims=True)
    accelerations = - r / np.power(distances_i0, 3) * m_star

    # Aceleración total
    a_s = accelerations.reshape(n, dim, 1)
    a = a_p + a_s
    return a


def acceleration_calculator_dist_weighted_star(r, m, K, r_crit, m_star):
    """


    :param m_star:
    :return:
    """
    n, dim, _ = r.shape

    # Aceleración debido a la intereacción planeta-planeta
    a_p = acceleration_calculator_dist_weighted(r, m, K, r_crit)

    # Aceleración debido a la intereacción planeta-estrella
    distances_i0 = np.linalg.norm(r, axis=1, keepdims=True)
    accelerations = - r / np.power(distances_i0, 3) * m_star

    # Aceleración total
    a_s = accelerations.reshape(n, dim, 1)
    a = a_p + a_s
    return a