# -*- coding: utf-8 -*-

from NBodyF.core.constants import den
from NBodyF.core.units import UA_to_LU
from NBodyF.core.forces import acceleration_calculator_dist_weighted_star, acceleration_calculator_dist_weighted_star_2
import numpy as np
from math import pi
import h5py
import os
import datetime
import json


def gen_random_planet(a_max, a_min, m_o, m_star, fm_min=1, fm_max=5, dim=2, density=den):
    """ Genera un planeta de forma aleatoria con orbita circular
        alrededor de una estrella de masa Mstar con masa entre fm_min * Mo
        y fm_max * Mo y semi eje major generado de forma uniforme entre a_max y
        a_min.

    :param a_max:
    :param a_min:
    :param m_o:
    :param m_star:
    :param fm_min:
    :param fm_max:
    :param dim:
    :param density:
    :return:
    """

    if dim != 2:
        raise NotImplementedError("Actualmente solo soporta simulación 2D")

    dist = (np.random.uniform(a_min, a_max, 1))
    phi = np.random.uniform(0, 2 * pi, 1)

    r = pol2cart(dist, phi)
    v = np.sqrt(m_star / dist) * np.array([- np.sin(phi), np.cos(phi)]) *  (1 - 0.1 * 0.1)
    m = m_o * np.random.uniform(fm_min, fm_max, 1)

    radius = (m / (4. / 3. * pi * density)) ** (1./3.)

    return r, v, m, radius, density


def gen_planet_orbital(a, e, m, m_star, density, dim=2):
    """ Genera un planeta a partir de los datos robitales y su masa.

    :param a_max:
    :param a_min:
    :param m_o:
    :param m_star:
    :param fm_min:
    :param fm_max:
    :param dim:
    :param density:
    :return:
    """

    if dim != 2:
        raise NotImplementedError("Actualmente solo soporta simulación 2D")

    dist = a
    phi = np.random.uniform(0, 2 * pi, 1)

    r = pol2cart(dist, phi)
    v = np.sqrt((m_star + m) / dist * (1 - e * e)) * np.array([- np.sin(phi), np.cos(phi)])

    radius = (m / (4. / 3. * pi * density)) ** (1./3.)

    return r, v, m, radius, density


def gen_planet_dinamical(r, v, m, m_star, density, dim=2):
    """ Genera un planeta a partir de los datos robitales y su masa.

    :param a_max:
    :param a_min:
    :param m_o:
    :param m_star:
    :param fm_min:
    :param fm_max:
    :param dim:
    :param density:
    :return:
    """

    if dim != 2:
        raise NotImplementedError("Actualmente solo soporta simulación 2D")

    r = np.array([[r[0]], [r[1]]])
    v = np.array([[v[0]], [v[1]]])

    radius = (m / (4. / 3. * pi * density)) ** (1./3.)

    return r, v, m, radius, density



def insert_js_model(modeljs, r, v, m, radius, density, m_star):
    r_js = list()
    v_js = list()
    m_js = list()
    rad_js = list()
    dens_js = list()

    rj, vj, mj, radiusj, denj = gen_planet_orbital(a=modeljs["ajup"],
                                                   e=modeljs["ejup"],
                                                   m=modeljs["mjup"],
                                                   m_star=m_star,
                                                   density=modeljs["djup"])

    rs, vs, ms, radiuss, dens = gen_planet_orbital(a=modeljs["asat"],
                                                   e=modeljs["esat"],
                                                   m=modeljs["msat"],
                                                   m_star=m_star,
                                                   density=modeljs["dsat"])

    r_js.append(rj)
    r_js.append(rs)
    v_js.append(vj)
    v_js.append(vs)
    m_js.append(mj)
    m_js.append(ms)
    rad_js.append(radiusj)
    rad_js.append(radiuss)
    dens_js.append(denj)
    dens_js.append(dens)

    r_js = np.array(r_js)
    v_js = np.array(v_js)
    m_js = np.array(m_js).reshape(2, 1)
    rad_js = np.array(rad_js).reshape(2, 1)
    dens_js = np.array(dens_js).reshape(2, 1)

    r = np.concatenate((r, r_js), axis=0)
    v = np.concatenate((v, v_js), axis=0)
    m = np.concatenate((m, m_js), axis=0)
    radius = np.concatenate((radius, rad_js), axis=0)
    density = np.concatenate((density, dens_js), axis=0)

    return r, v, m, radius, density


def pol2cart(rho, phi):
    """ Conversion de coordenadas polares a cartesianas

    :param rho:
    :param phi:
    :return:
    """

    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return np.array([x, y])




def calc_dt(r, v, standard_dt=0.001):
    """


    :param r:
    :param v:
    :param standard_dt:
    :param reg_dt:
    :return:
    """

    n, dim, _ = r.shape
    rij = r.reshape(1, n, dim) - r.reshape(n, 1, dim)
    vij = v.reshape(1, n, dim) - v.reshape(n, 1, dim)

    rij_rij = np.sum(rij * rij, axis = 2)
    rij_rij[np.arange(n), np.arange(n)] = 1

    rij_vij = np.sum(rij * vij, axis=2)
    rij_vij[np.arange(n), np.arange(n)] = 1

    dt_calc = 0.25 * np.min(np.abs(rij_rij / rij_vij))
    dt = min(dt_calc, standard_dt)

    return dt


def remove_bodies(r, v, m):
    """


    :param r:
    :param v:
    :param m:
    :return:
    """
    pass


def calc_orbital_elements(r, v, m):
    """


    :param r:
    :param v:
    :param m:
    :return:
    """
    pass

def calc_semi_major_axis(r, v, m, m_star):
    """
    Calculates an object's orbital semi-major axis given its Cartesian coords.

    :param r:
    :param v:
    :param m:
    :param m_star:
    :return:
    """
    h = r[:, 0] * v[:, 1] - r[:, 1] * v[:, 0]
    h2 = h * h

    gm = (m + m_star)
    n_r = np.linalg.norm(r, axis=1)
    v2 = np.sum(v * v, axis=1)

    a = n_r * gm / (2 * gm - n_r * v2) # Eq. 3.45

    e = np.sqrt(1 - h2 / gm * (1. / a))

    a[e >= 1] = n_r[e >= 1]

    return a


def calc_eccentricity(r, v, m, m_star, tol=1e-6):
    """
    Calculates Keplerian orbital elements

    :param r:
    :param v:
    :param m:
    :param m_star:
    :return:
    """
    h = r[:, 0] * v[:, 1] - r[:, 1] * v[:, 0]
    h2 = h * h
    gm = (m + m_star)

    n_r = np.linalg.norm(r, axis=1)
    v2 = np.sum(v * v, axis=1)

    e2 = 1 + h2 / gm * (v2 / gm - 2. / n_r)
    e2[np.abs(e2) < tol] = 0
    e = np.sqrt(e2)

    e[e < tol] = 0

    return e


def calc_hill_radius(r, v, m, m_star, tol=1e-6):
    """


    :param r:
    :param v:
    :param m:
    :param m_star:
    :param tol:
    :return:
    """
    gm = (m + m_star)
    n_r = np.linalg.norm(r, axis=1)
    v2 = np.sum(v * v, axis=1)

    a = n_r * gm / (2 * gm - n_r * v2)  # Eq. 3.45

    h = r[:, 0] * v[:, 1] - r[:, 1] * v[:, 0]
    h2 = h * h

    e = np.sqrt(1 - h2 / gm * (1. / a))

    e[e < tol] = 0

    a[e >= 1] = n_r[e >= 1]

    hill = a * (m / (3 * m_star)) ** (1. / 3)

    return hill


def calc_rcrit(r, v, m, m_star, dt, tol=1e-6, n1=3., n2=0.3): #n1=3, n2=0.4): #n1=7., n2=1.):#3, n2=0.4):
    gm = (m + m_star)
    n_r = np.linalg.norm(r, axis=1)
    v2 = np.sum(v * v, axis=1)

    a = n_r * gm / (2 * gm - n_r * v2)  # Eq. 3.45

    h = r[:, 0] * v[:, 1] - r[:, 1] * v[:, 0]
    h2 = h * h

    e = np.sqrt(1 - h2 / gm * (1. / a))

    e[e < tol] = 0.

    a[e >= 1] = n_r[e >= 1]

    hill = a * (m / (3 * m_star)) ** (1. / 3)

    a_min = np.min(a)

    v_max = (m_star / a_min) ** (0.5)

    r_crit = np.maximum(n1 * hill, n2 * dt * v_max)

    return r_crit[:, 0]


def find_minimun(d0, d1, d0t, d1t, dt):
    """
    Calculates minimum value of a quantity D, within an interval H, given initial
    and final values D0, D1, and their derivatives D0T, D1T, using third-order
    (i.e. cubic) interpolation.
    """

    temp = 6. * (d0 - d1)
    a = temp + 3. * dt * (d0t + d1t)
    b = - temp - 2. * dt * (2. * d0t + d1t)
    c = dt * d0t

    temp = b * b - 4. * a * c

    if temp >= 0:
        t1 = 0.5 * (- b + np.sqrt(temp)) / a
        t2 = 0.5 * (- b - np.sqrt(temp)) / a

        t1 = min(max(0, t1), 1)
        t2 = min(max(0, t2), 1)

        temp1 = 1 - t1
        dist1 = t1 * t1 * ((3 - 2 * t1) * d1 - temp1 * dt * d1t) + \
                temp1 * temp1 * ((1 + 2 * t1) * d0 + t1 * dt * d0t)

        temp2 = 1 - t2
        dist2 = t2 * t2 * ((3 - 2 * t2) * d1 - temp2 * dt * d1t) + \
                temp2 * temp2 * ((1 + 2 * t2) * d0 + t2 * dt * d0t)
    else:
        t1 = 0.
        t2 = 1.
        dist1 = d0
        dist2 = d1

    dmin = min(dist1, dist2)

    if dmin == dist1:
        return dmin, t1
    elif dmin == dist2:
        return dmin, t2


def close_encounters(r0, r, v0, v, dt, m, m_star):
    xmin = np.minimum(r0[:, 0], r[:, 0])[:, 0]
    xmax = np.maximum(r0[:, 0], r[:, 0])[:, 0]
    ymin = np.minimum(r0[:, 1], r[:, 1])[:, 0]
    ymax = np.maximum(r0[:, 1], r[:, 1])[:, 0]

    # If velocity changes sign, do an interpolation
    v0byv = v0 * v

    inter_x = np.where((v0byv[:, 0] < 0) == True)[0]
    inter_y = np.where((v0byv[:, 1] < 0) == True)[0]

    for i in inter_x:
        temp = (v0[i, 0] * r[i, 0] - v[i, 0] * r0[i, 0] - 0.5 * dt * v[i, 0] * v0[i, 0]) / (v0[i, 0] - v[i, 0])
        xmin[i] = min(xmin[i], temp)
        xmax[i] = max(xmax[i], temp)

    for i in inter_y:
        temp = (v0[i, 1] * r[i, 1] - v[i, 1] * r0[i, 1] - 0.5 * dt * v[i, 1] * v0[i, 1]) / (v0[i, 1] - v[i, 1])
        ymin[i] = min(ymin[i], temp)
        ymax[i] = max(ymax[i], temp)

    # Adjust values by symplectic close-encounter distance

    rcrit = calc_rcrit(r, v, m, m_star, dt)  # Se calcula respecto r0 ?
    n = len(rcrit)
    ce = np.zeros(n, dtype=bool)

    xmin -= rcrit
    xmax += rcrit
    ymin -= rcrit
    ymax += rcrit

    ce_index = list()
    d_min_l = list()
    t_d_min = list()

    # Identify pairs whose X-Y boxes overlap, and calculate minimum separation
    for i in range(n):
        for j in range(i + 1, n):
            if (xmax[i] >= xmin[j]) and (xmax[j] >= xmin[i]) and (ymax[j] >= ymin[i]) and (ymax[i] >= ymin[j]):

                # Determine the maximum separation that would qualify as an encounter
                rc = max(rcrit[i], rcrit[j])
                rc2 = rc * rc

                # Calculate initial and final separations
                dr0 = r0[i, :] - r0[j, :]
                dr1 = r[i, :] - r[j, :]

                d0 = np.sum(dr0 * dr0)
                d1 = np.sum(dr1 * dr1)

                # Check for a possible minimum in between
                dv0 = v0[i, :] - v0[j, :]
                dv1 = v[i, :] - v[j, :]

                d0t = np.sum(dr0 * dv0) * 2.
                d1t = np.sum(dr1 * dv1) * 2.

                # If separation derivative changes sign, find the minimum separation
                dmin = np.inf
                if d0t * d1t < 0:
                    dmin, t = find_minimun(d0, d1, d0t, d1t, dt)

                # If minimum separation is small enough, flag this as a possible encounter
                dmin = min(d0, d1, dmin)

                if dmin == d0:
                    t = 0
                elif dmin == d1:
                    t = 1

                if (dmin * dmin) <= rc2:
                    ce[i] = True
                    ce[j] = True
                    ce_index.append([i, j])
                    d_min_l.append(dmin)
                    t_d_min.append(t)
    return ce, ce_index, d_min_l, t_d_min


def dy_f(rc, k, m, m_star):
    def temp(y):
        return np.array([y[1], acceleration_calculator_dist_weighted_star(y[0], m, k, rc, m_star)])
    return temp


def dy_f_2(w, m, m_star):
    def temp(y):
        return np.array([y[1], acceleration_calculator_dist_weighted_star_2(y[0], m, w, m_star)])
    return temp


def remover(dist_min, dist_max):
    """ Remueve obejetos cverca de la estrella, con velocidad amyor a la de escape, divergentes"""
    def temp(r, v, m, m_star):
        # Remove Obejcts Near to Star
        distances_i0 = np.linalg.norm(r, axis=1, keepdims=False)
        index_cs = distances_i0 < dist_min
        index_cs = index_cs.nonzero()[0]

        index_dv = distances_i0 > dist_max
        index_dv = index_dv.nonzero()[0]

        new_mstar = m_star + np.sum(m[index_cs])

        return np.concatenate((index_cs, index_dv)), new_mstar

    return temp


