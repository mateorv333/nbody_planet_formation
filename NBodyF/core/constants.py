# -*- coding: utf-8 -*-
import math

from NBodyF.core.units import MU, kg_to_MU, yr_to_sec, m_to_LU, g_to_MU

""" ---------------------------------------------------------------------- """
""" Constants """
""" ---------------------------------------------------------------------- """

""" Solar Mass in grams """
M_solar = 1.98911 * 10 ** 33

""" Mercury Mass in grams"""
M_merc = 3.285 * 10 ** 26

""" Earth Mass in grams"""
M_earth = 5.972 * 10 ** 27

""" Jupyter Mass """
M_jup = 1.898 * 10 ** 30 * g_to_MU

""" Jupyter Density """
Den_jup = 1336 * kg_to_MU / (m_to_LU ** 3)

""" Saturn Mass """
M_sat = 5.683 * 10 ** 29 * g_to_MU

""" Uranus Mass """
M_ura = 8.681 * 10 ** 28 * g_to_MU

""" Neptune Mass """
M_nep = 1.024 * 10 ** 29 * g_to_MU

""" Saturn Density """
Den_sat = 690 * kg_to_MU / (m_to_LU ** 3)

""" Solar Radius in Astronomical Unit """
Rs = 0.00465047

""" Astronomical Unit in cm """
UA = 14959787070000.0

""" ---------------------------------------------------------------------- """
""" Collision Parameters """
""" ---------------------------------------------------------------------- """

""" Impact Strength """
S = 3e7 * kg_to_MU * yr_to_sec ** 2 / m_to_LU

""" Sound Velocity at Basalt """
vc = 363 * yr_to_sec * m_to_LU

""" Density of the Body, assume constant """
den = 3e3 * kg_to_MU / (m_to_LU ** 3)

""" Density used in Chambers Model Fragmentation, assume constant """
den1 = 1e3 * kg_to_MU / (m_to_LU ** 3)

""" Restitution Coefficient """
ci = 0.7

""" Modified Restitution Coefficient """
cii = 0.5

""" Maximum Rebound Velocity """
Vc = 55 * m_to_LU * yr_to_sec

""" Calculated Maximum Rebound Velocity """
Vcc = 2.*S/(vc*den)

""" Ejecta Velocity Coefficient """
cej = 3e6 * (m_to_LU * yr_to_sec) ** (9./4.)

""" Mass excavated coefficient """
Kms = 1. / 1e7 * (1. / (m_to_LU * yr_to_sec)) ** 2

""" Angule orientation of debris velocities """
phi = math.pi / 4.0

""" ---------------------------------------------------------------------- """

