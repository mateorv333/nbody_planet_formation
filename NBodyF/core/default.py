# -*- coding: utf-8 -*-
import os
from NBodyF.core.units import UA_to_LU
from NBodyF.core.constants import M_jup, M_sat, Den_jup, Den_sat

config_folder = os.path.join('config')
data_folder = os.path.join('data')
info_folder = os.path.join('info')
simulation_folder = os.path.join('simulation')
readme = os.path.join('README.md')
path_params_simul = os.path.join(config_folder, 'parameters_simulation.yaml')
path_init_config = os.path.join(config_folder, 'init_config.h5')

#Define Jupiter-Saturn Model
js_model = {
            "JS": dict(ajup=5.204267 * UA_to_LU,  # Jupiter and Saturn Actual
                       asat=9.5820172 * UA_to_LU,
                       ejup=0.05648,
                       esat=0.0,
                       mjup=M_jup,
                       msat=M_sat,
                       djup=Den_jup,
                       dsat=Den_sat),
            "CJS" : dict(ajup=5.45 * UA_to_LU,  # Circular Jupiter and Saturn
                         asat=8.18 * UA_to_LU,
                         ejup=0.0,
                         esat=0.0,
                         mjup=M_jup,
                         msat=M_sat,
                         djup=Den_jup,
                         dsat=Den_sat),
            "CJSECC" : dict(ajup=5.45 * UA_to_LU,  # Semi-Circular Jupiter and Saturn
                            asat=8.18 * UA_to_LU,
                            ejup=0.02,
                            esat=0.03,
                            mjup=M_jup,
                            msat=M_sat,
                            djup=Den_jup,
                            dsat=Den_sat),
            "EJS" : dict(ajup=5.25 * UA_to_LU,  # Eccentric Jupiter and Saturn
                         asat=9.54 * UA_to_LU,
                         ejup=0.05,
                         esat=0.06,
                         mjup=M_jup,
                         msat=M_sat,
                         djup=Den_jup,
                         dsat=Den_sat),
            "EEJS" : dict(ajup=5.25 * UA_to_LU,  # Extra Eccentric Jupiter and Saturn
                          asat=9.54 * UA_to_LU,
                          ejup=0.1,
                          esat=0.1,
                          mjup=M_jup,
                          msat=M_sat,
                          djup=Den_jup,
                          dsat=Den_sat),
            "JSRES" : dict(ajup=5.43 * UA_to_LU,  # Jupiter and Saturn in RESonance
                           asat=7.3 * UA_to_LU,
                           ejup=0.005,
                           esat=0.01,
                           mjup=M_jup,
                           msat=M_sat,
                           djup=Den_jup,
                           dsat=Den_sat),
            "JSRESECC" : dict(ajup=5.43 * UA_to_LU,  # Jupiter and Saturn in RESonance on ECCentric orbits
                              asat=7.3 * UA_to_LU,
                              ejup=0.03,
                              esat=0.03,
                              mjup=M_jup,
                              msat=M_sat,
                              djup=Den_jup,
                              dsat=Den_sat)
            }
