# -*- coding: utf-8 -*-

from NBodyF.core.constants import M_merc, den
from NBodyF.core.units import g_to_MU
from NBodyF.core.utils import gen_random_planet, gen_planet_orbital, gen_planet_dinamical
import numpy as np
import math


def random_system(n_g, n_p, a_min, a_max, m_star=1, m_o=M_merc*g_to_MU, dim=2, density=den, fmg_min=1, fmg_max=5, fmp_min=0.01,
                  fmp_max=0.0333):
	""" Inicialización aleatoria de un sistema

		n_g : Número de planetas del orden de Mo.
		n_p : 1 o 2 ordenes de magnitud menores a Mo
		a_max : semi-major axis max value 1AU
		a_min : semi-major axis min value 1AU

		:param n_g:
		:param n_p:
		:param a_max:
		:param a_min:
		:param dim:
		:param m_star:
		:param m_o:
		:param density:
		:return:
	"""
	r = list()
	v = list()
	m = list()
	rad = list()
	dens = list()

	# Generación de planetoides grandes
	for _ in range(n_g):
		ri, vi, mi, radius_i, density_i = gen_random_planet(a_max=a_max, a_min=a_min, m_o=m_o, m_star=m_star, fm_min=fmg_min,
															fm_max=fmg_max, dim=dim, density=density)
		r.append(ri)
		v.append(vi)
		m.append(mi)
		rad.append(radius_i)
		dens.append(density_i)

	# Generación de planetoides pequeños
	# TODO: Que es grande y pequeño en este contexto
	for _ in range(n_p):
		ri, vi, mi, radius_i, density_i = gen_random_planet(a_max=a_max, a_min=a_min, m_o=m_o, m_star=m_star, fm_min=fmp_min,
															fm_max=fmp_max, dim=dim, density=density)

		r.append(ri)
		v.append(vi)
		m.append(mi)
		rad.append(radius_i)
		dens.append(density_i)

	r = np.array(r)
	v = np.array(v)
	m = np.array(m)
	rad = np.array(rad)
	dens = np.array(dens).reshape(m.shape)

	return r, v, m, rad, dens

# TODO: Description of cration of a system of palnets and merge them into one with all description and planets and save

# TODO: Add other properties Radius, Density, Density ...


def system_from_orbital_elements(planets, mstar):
	r_js = list()
	v_js = list()
	m_js = list()
	rad_js = list()
	dens_js = list()

	n = len(planets)

	for plt in planets:

		rj, vj, mj, radiusj, denj = gen_planet_orbital(a=plt["a"],
													   e=plt["e"],
													   m=plt["m"],
													   m_star=mstar,
													   density=plt["den"])

		r_js.append(rj)
		v_js.append(vj)
		m_js.append(mj)
		rad_js.append(radiusj)
		dens_js.append(denj)

	r = np.array(r_js)
	v = np.array(v_js)
	m = np.array(m_js).reshape(n, 1)
	radius = np.array(rad_js).reshape(n, 1)
	density = np.array(dens_js).reshape(n, 1)

	return r, v, m, radius, density


def system_from_dinamical_elements(planets, mstar):
	r_js = list()
	v_js = list()
	m_js = list()
	rad_js = list()
	dens_js = list()

	n = len(planets)

	for plt in planets:

		rj, vj, mj, radiusj, denj = gen_planet_dinamical(r=plt["r"],
													     v=plt["v"],
													     m=plt["m"],
													     m_star=mstar,
													     density=plt["den"])

		r_js.append(rj)
		v_js.append(vj)
		m_js.append(mj)
		rad_js.append(radiusj)
		dens_js.append(denj)

	r = np.array(r_js)
	v = np.array(v_js)
	m = np.array(m_js).reshape(n, 1)
	radius = np.array(rad_js).reshape(n, 1)
	density = np.array(dens_js).reshape(n, 1)

	return r, v, m, radius, density