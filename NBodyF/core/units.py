# -*- coding: utf-8 -*-
import math

""" ---------------------------------------------------------------------- """
""" Unit System """
""" ---------------------------------------------------------------------- """

""" Mass Unit (MU) un grams """
MU = 1.98911 * 10 ** 33

""" Length Unit (LU) in cm """
LU = 5.0939 * 10 ** 13

""" ---------------------------------------------------------------------- """
""" Convertion Factors """
""" ---------------------------------------------------------------------- """

""" Convertion Grams to Solar Mass """
g_to_MU = 1. / MU

""" Convertion Kilogram to Solar Mass """
kg_to_MU = 1000. / MU

""" Convertion Year to Seconds """
yr_to_sec = 31556926.

""" Convertion Centimeter to LU """
cm_to_LU = 1. / LU

""" Convertion Meter to LU """
m_to_LU = 100. / LU

""" Convertion Kilometer to LU """
km_to_LU = 1000. * 100. / LU

""" Convertion Kilometer by second to LU by year """
kms_to_LUyr = km_to_LU * yr_to_sec

""" Convertion Factor UA to LU """
UA_to_LU = 14959787070000.0 * cm_to_LU

""" Convertion Factor LU to UA """
LU_to_UA = 1. / UA_to_LU

""" Convertion Factor MU to Earth Masses """
MU_to_ME = MU / (5.972 * 10 ** 27)

