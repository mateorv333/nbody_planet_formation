# -*- coding: utf-8 -*-
import yaml
import warnings
import os
import math
from tqdm import tqdm
from math import pi
import numpy as np

from NBodyF.utils.args import *
from NBodyF.utils.io import create_hdf5, insert_group_hdf5, load_system
from NBodyF.core.integrators import hybrid_symplectic_integrator, leapfrog_step, hybrid_symplectic_integrator_frag
from NBodyF.core.forces import acceleration_calc_star
from NBodyF.core.utils import remover, insert_js_model
from NBodyF.core.units import UA_to_LU
from NBodyF.core.default import js_model


if __name__ == '__main__':
    overwrite = False

    args = runner.parse_args()
    path_params = args.path_params
    path_init = args.path_init_config

    if args.overwrite:
        overwrite = True

    with open(path_params) as file:
        parameters = yaml.load(file)

    # Parametros Obligatorioas
    name = parameters['name']
    tf = parameters['tf']
    dt = parameters['dt']
    n1 = parameters['n1'] if 'n1' in parameters.keys() else 3.
    n2 = parameters['n2'] if 'n2' in parameters.keys() else 0.4
    record = parameters['record']

    ## Chequear que el record sea un numero entero
    assert (type(record) == int)

    # Definir Removedor de Objetos
    dmin = parameters['dmin'] if 'dmin' in parameters.keys() else 0.2 * UA_to_LU
    remov = remover(dmin)


    # Definir Integrador
    integ = parameters['integrator']
    if integ == "HS_BS":
        integrator = hybrid_symplectic_integrator
    elif integ == "LF":
        integrator = leapfrog_step(acceleration_calc_star)
    elif  integ == "HS_BS_FRAG":
        integrator = hybrid_symplectic_integrator_frag
    else:
        raise NotImplementedError

    # Inicialización de Sistema
    file_path = os.path.join(simulation_folder, "template_hb_JS" + ".h5")
    r, v, m, radius, ms, tm, sys_par = load_system(file_path, iteration=-1, radius=True)

    r = r[0]
    v = v[0]
    m = m[0]
    radius = radius[0]

    m_star = ms[0]
    m_o = sys_par["m_o"]

    if not os.path.exists(file_path):
        raise ValueError("El archivo {} no existe.".format(file_path))

    # Inicialización de Contadores
    time = sys_par["last_time"]
    last_iter = sys_par["last_iteration"] * record

    # Rerun Params
    extra_time = 500
    extra_iter = math.ceil(extra_time / dt)

    # Counters
    nb = 0 # Número de Objetos
    ndiv = 0 # Número de Objetos Divergentes
    nstar = 0 # Número de Objetos absorbido por la estrella
    ncol_reb = 0 # Contador de colisiones-rebound
    ncol_frag = 0  # Contador de colisiones-fragmentacion
    ncol_merge = 0  # Contador de colisiones-merge

    print("El número total de iteraciones es {}".format(extra_iter))

    for i in tqdm(range(last_iter + 1, last_iter + extra_iter)):
        if i%record == 0:
              insert_group_hdf5(file_path, r, v, m, m_star, radius = radius, iteration = i // record, time = time, dt = dt)

        # HB
        # r, v = integrator(r, v, m, dt, m_star)

        #HB Frag
        r, v, m, radius = integrator(r, v, m, radius, dt, m_star)

        # Delete Objects
        index, m_star = remov(r, v, m, m_star)
        if len(index) > 0:
            r = np.delete(r, index, axis=0)
            v = np.delete(v, index, axis=0)
            m = np.delete(m, index, axis=0)
            radius = np.delete(radius, index, axis=0)

        time += dt
